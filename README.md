Vogue Recovery Center has locations in Arizona, Nevada and California. The Nevada facility provides all levels of substance abuse treatment with the Arizona location providing residential care. Vogue Recovery Center is able to care for patients recovering from any substance in an aesthetically pleasing comfortable environment that provides quality evidence-based therapies.

Website: https://www.voguerecoverycenter.com/
